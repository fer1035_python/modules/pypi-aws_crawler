#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""Crawl through AWS accounts in an organization using master assumed role."""
import __init__

if __name__ == "__main__":
    __init__.main()
